<?php 

require ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$sheep = new animal ("Shaun");

echo "Name :  $sheep->name";
echo "<br>";
echo "Legs : $sheep->legs";
echo "<br>";
echo "Cold Blooded : $sheep->cold_blooded";
echo "<br>";
echo "<br>";

$sungokong = new ape ("Kera Sakti");

echo "Name : $sungokong->name";
echo "<br>";
echo "Legs : $sungokong->legs";
echo "<br>";
$sungokong->yell();
echo "<br>";
echo "Cold Blooded : $sungokong->cold_blooded";
echo "<br>";
echo "<br>";

$kodok = new frog ("Buduk");

echo "Name : $kodok->name";
echo "<br>";
echo "Legs : $kodok->legs";
echo "<br>";
$kodok->jump();
echo "<br>";
echo "Cold Blooded : $kodok->cold_blooded";


?>